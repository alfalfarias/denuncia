package views;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JToolBar;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.BoxLayout;


public class Main extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4708253138929074018L;
	private JPanel contentPane;
	private JPanel panel;
	private JButton btnDenunciantes;
	private JButton btnVictimas;
	private JButton btnDenuncias;
	private JButton btnCrearNuevaDenuncia;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Main frame = new Main();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Main() {
		setTitle("Denuncias Universitarias V 1.0");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 600, 400);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JToolBar toolBar = new JToolBar();
		toolBar.setFloatable(false);
		contentPane.add(toolBar, BorderLayout.NORTH);
		
		btnDenunciantes = new JButton("Denunciantes");
		btnDenunciantes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				panel.removeAll();
				DenuncianteRead denuncianteRead = new DenuncianteRead();
				panel.add(denuncianteRead, BorderLayout.CENTER);
				panel.revalidate();
				panel.repaint();
			}
		});
		toolBar.add(btnDenunciantes);
		
		btnVictimas = new JButton("Victimas");
		btnVictimas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				panel.removeAll();
				VictimaRead victimaRead = new VictimaRead();
				panel.add(victimaRead, BorderLayout.CENTER);
				panel.revalidate();
				panel.repaint();
			}
		});
		toolBar.add(btnVictimas);
		
		btnDenuncias = new JButton("Denuncias");
		btnDenuncias.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				panel.removeAll();
				DenunciaRead denunciaRead = new DenunciaRead();
				panel.add(denunciaRead, BorderLayout.CENTER);
				panel.revalidate();
				panel.repaint();
			}
		});
		toolBar.add(btnDenuncias);
		
		panel = new JPanel();
		contentPane.add(panel, BorderLayout.CENTER);
		
		btnCrearNuevaDenuncia = new JButton("CREAR NUEVA DENUNCIA");
		btnCrearNuevaDenuncia.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				DenunciaCreate denunciaCreate=new DenunciaCreate();
				denunciaCreate.setLocationRelativeTo(panel);
				denunciaCreate.setModal(true);
				denunciaCreate.setVisible(true);
				inicializar();
			}
		});
		contentPane.add(btnCrearNuevaDenuncia, BorderLayout.SOUTH);
		inicializar();
	}
	public void inicializar(){
		panel.removeAll();
		panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
		DenuncianteRead denuncianteRead = new DenuncianteRead();
		panel.add(denuncianteRead);
		panel.revalidate();
		panel.repaint();
	}
}
