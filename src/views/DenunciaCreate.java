package views;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.border.TitledBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JOptionPane;
import javax.swing.JTextPane;
import javax.swing.JScrollPane;
import javax.swing.JEditorPane;
import javax.swing.UIManager;

import java.awt.Color;

import javax.swing.JSpinner;
import javax.swing.SpinnerDateModel;

import database.QueryController;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Calendar;

public class DenunciaCreate extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JTextField dNombres;
	private JTextField dApellidos;
	private JTextField dCedula;
	private JTextField vNombres;
	private JTextField vApellidos;
	private JTextField vCedula;
	private JEditorPane dHechos;
	private JSpinner dFecha;
	private JButton okButton;
	private JEditorPane dDomicilio;
	private JEditorPane vDomicilio;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			DenunciaCreate dialog = new DenunciaCreate();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public DenunciaCreate() {
		setTitle("Crear nueva denuncia");
		setBounds(100, 100, 700, 419);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Datos del denunciante", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Datos de la victima", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		
		JLabel label = new JLabel("Nombres");
		
		vNombres = new JTextField();
		vNombres.setColumns(10);
		
		JLabel label_1 = new JLabel("Apellidos");
		
		vApellidos = new JTextField();
		vApellidos.setColumns(10);
		
		JLabel label_2 = new JLabel("C\u00E9dula");
		
		vCedula = new JTextField();
		vCedula.setColumns(10);
		
		JLabel label_3 = new JLabel("Domicilio");
		
		JScrollPane scrollPane_1 = new JScrollPane();
		GroupLayout gl_panel_1 = new GroupLayout(panel_1);
		gl_panel_1.setHorizontalGroup(
			gl_panel_1.createParallelGroup(Alignment.TRAILING)
				.addGap(0, 277, Short.MAX_VALUE)
				.addGroup(gl_panel_1.createSequentialGroup()
					.addGroup(gl_panel_1.createParallelGroup(Alignment.TRAILING)
						.addGroup(gl_panel_1.createSequentialGroup()
							.addContainerGap()
							.addComponent(scrollPane_1, GroupLayout.DEFAULT_SIZE, 245, Short.MAX_VALUE))
						.addGroup(gl_panel_1.createParallelGroup(Alignment.TRAILING)
							.addGroup(gl_panel_1.createSequentialGroup()
								.addContainerGap()
								.addComponent(vCedula, GroupLayout.DEFAULT_SIZE, 245, Short.MAX_VALUE))
							.addGroup(gl_panel_1.createSequentialGroup()
								.addContainerGap()
								.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
									.addComponent(vNombres, GroupLayout.DEFAULT_SIZE, 110, Short.MAX_VALUE)
									.addComponent(label))
								.addGap(16)
								.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
									.addComponent(label_1)
									.addComponent(vApellidos, GroupLayout.DEFAULT_SIZE, 116, Short.MAX_VALUE)))
							.addGroup(Alignment.LEADING, gl_panel_1.createSequentialGroup()
								.addContainerGap()
								.addComponent(label_2)))
						.addGroup(Alignment.LEADING, gl_panel_1.createSequentialGroup()
							.addContainerGap()
							.addComponent(label_3)))
					.addContainerGap())
		);
		gl_panel_1.setVerticalGroup(
			gl_panel_1.createParallelGroup(Alignment.LEADING)
				.addGap(0, 213, Short.MAX_VALUE)
				.addGroup(gl_panel_1.createSequentialGroup()
					.addGroup(gl_panel_1.createParallelGroup(Alignment.BASELINE)
						.addComponent(label_1)
						.addComponent(label))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panel_1.createParallelGroup(Alignment.BASELINE)
						.addComponent(vNombres, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(vApellidos, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(label_2)
					.addGap(1)
					.addComponent(vCedula, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addComponent(label_3)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(scrollPane_1, GroupLayout.DEFAULT_SIZE, 67, Short.MAX_VALUE)
					.addGap(5))
		);
		
		vDomicilio = new JEditorPane();
		scrollPane_1.setViewportView(vDomicilio);
		panel_1.setLayout(gl_panel_1);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBorder(new TitledBorder(null, "Datos de la denuncia", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		GroupLayout gl_contentPanel = new GroupLayout(contentPanel);
		gl_contentPanel.setHorizontalGroup(
			gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(Alignment.TRAILING, gl_contentPanel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.TRAILING)
						.addComponent(panel_2, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 561, Short.MAX_VALUE)
						.addGroup(gl_contentPanel.createSequentialGroup()
							.addComponent(panel, GroupLayout.DEFAULT_SIZE, 277, Short.MAX_VALUE)
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(panel_1, GroupLayout.DEFAULT_SIZE, 274, Short.MAX_VALUE)))
					.addContainerGap())
		);
		gl_contentPanel.setVerticalGroup(
			gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPanel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
						.addComponent(panel_1, GroupLayout.PREFERRED_SIZE, 213, GroupLayout.PREFERRED_SIZE)
						.addComponent(panel, GroupLayout.PREFERRED_SIZE, 213, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(panel_2, GroupLayout.DEFAULT_SIZE, 96, Short.MAX_VALUE)
					.addContainerGap())
		);
		
		JLabel lblRelatoDeLos = new JLabel("Relato de los hechos que motivan la denuncia");
		
		JScrollPane scrollPane_2 = new JScrollPane();
		
		JLabel lblFecha = new JLabel("Fecha");
		
		dFecha = new JSpinner();
		dFecha.setModel(new SpinnerDateModel(new Date(1525485842654L), new Date(1525485842654L), null, Calendar.YEAR));
		GroupLayout gl_panel_2 = new GroupLayout(panel_2);
		gl_panel_2.setHorizontalGroup(
			gl_panel_2.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_2.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panel_2.createParallelGroup(Alignment.LEADING)
						.addComponent(scrollPane_2, GroupLayout.DEFAULT_SIZE, 529, Short.MAX_VALUE)
						.addGroup(gl_panel_2.createSequentialGroup()
							.addComponent(lblRelatoDeLos)
							.addGap(68)
							.addComponent(lblFecha)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(dFecha, GroupLayout.DEFAULT_SIZE, 210, Short.MAX_VALUE)))
					.addContainerGap())
		);
		gl_panel_2.setVerticalGroup(
			gl_panel_2.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_2.createSequentialGroup()
					.addGroup(gl_panel_2.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblRelatoDeLos)
						.addComponent(lblFecha)
						.addComponent(dFecha, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
					.addComponent(scrollPane_2, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
					.addContainerGap())
		);
		
		dHechos = new JEditorPane();
		scrollPane_2.setViewportView(dHechos);
		panel_2.setLayout(gl_panel_2);
		
		JLabel lblNombres = new JLabel("Nombres");
		
		dNombres = new JTextField();
		dNombres.setColumns(10);
		
		JLabel lblApellidos = new JLabel("Apellidos");
		
		dApellidos = new JTextField();
		dApellidos.setColumns(10);
		
		JLabel lblCdula = new JLabel("C\u00E9dula");
		
		dCedula = new JTextField();
		dCedula.setColumns(10);
		
		JLabel lblDomicilio = new JLabel("Domicilio");
		
		JScrollPane scrollPane = new JScrollPane();
		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(
			gl_panel.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_panel.createSequentialGroup()
					.addGroup(gl_panel.createParallelGroup(Alignment.TRAILING)
						.addGroup(gl_panel.createSequentialGroup()
							.addContainerGap()
							.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 245, Short.MAX_VALUE))
						.addGroup(gl_panel.createParallelGroup(Alignment.TRAILING)
							.addGroup(gl_panel.createSequentialGroup()
								.addContainerGap()
								.addComponent(dCedula, GroupLayout.DEFAULT_SIZE, 245, Short.MAX_VALUE))
							.addGroup(gl_panel.createSequentialGroup()
								.addContainerGap()
								.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
									.addComponent(dNombres, GroupLayout.DEFAULT_SIZE, 110, Short.MAX_VALUE)
									.addComponent(lblNombres))
								.addGap(19)
								.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
									.addComponent(lblApellidos)
									.addComponent(dApellidos, GroupLayout.DEFAULT_SIZE, 116, Short.MAX_VALUE)))
							.addGroup(Alignment.LEADING, gl_panel.createSequentialGroup()
								.addContainerGap()
								.addComponent(lblCdula)))
						.addGroup(Alignment.LEADING, gl_panel.createSequentialGroup()
							.addContainerGap()
							.addComponent(lblDomicilio)))
					.addContainerGap())
		);
		gl_panel.setVerticalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblApellidos)
						.addComponent(lblNombres))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
						.addComponent(dNombres, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(dApellidos, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(lblCdula)
					.addGap(1)
					.addComponent(dCedula, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addComponent(lblDomicilio)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 67, Short.MAX_VALUE)
					.addGap(5))
		);
		
		dDomicilio = new JEditorPane();
		scrollPane.setViewportView(dDomicilio);
		panel.setLayout(gl_panel);
		contentPanel.setLayout(gl_contentPanel);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				okButton = new JButton("Crear");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						try {
							String dnombres = dNombres.getText().toString();
							String dapellidos = dApellidos.getText().toString();
							String dcedula = dCedula.getText().toString();
							String ddomicilio = dDomicilio.getText().toString();
							String denunciante_values = "(NULL, '"+dnombres+"', '"+dapellidos+"', '"+dcedula+"', '"+ddomicilio+"')";
							
							String vnombres = vNombres.getText().toString();
							String vapellidos = vApellidos.getText().toString();
							String vcedula = vCedula.getText().toString();
							String vdomicilio = vDomicilio.getText().toString();
							String victima_values = "(NULL, '"+vnombres+"', '"+vapellidos+"', '"+vcedula+"', '"+vdomicilio+"')";

							String dhechos = dHechos.getText().toString();
							String dfecha = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(dFecha.getValue());
							String denuncia_values = "'"+dhechos+"', '"+dfecha+"'";

							QueryController query=new QueryController();
							query.denunciaCreate(denunciante_values, victima_values, denuncia_values);
							query.close();
							JOptionPane.showMessageDialog(
									   contentPanel,
									   "�DATOS PROCESADOS!");
							dispose();
						}
						catch (Exception e){
							JOptionPane.showMessageDialog(
									   contentPanel,
									   e.getMessage()+"�Error al procesar datos, verifique los campos!");
						}
						System.out.println("DD Nombres: " +dNombres.getText().toString()+ " Apellidos: "+dApellidos.getText().toString()+" Cedula: " +dCedula.getText().toString()+ "Domicilio: "+dDomicilio.getText().toString());
						System.out.println("VV Nombres: " +vNombres.getText().toString()+ " Apellidos: "+vApellidos.getText().toString()+" Cedula: " +vCedula.getText().toString()+ "Domicilio: "+vDomicilio.getText().toString());
						System.out.println("dd Fecha: " +dFecha.getValue().toString()+ " Hechos: "+dHechos.getText().toString());
						System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(dFecha.getValue()));
						
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancelar");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						dispose();
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
	}
}
