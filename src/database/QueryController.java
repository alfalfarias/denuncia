package database;

import java.sql.SQLException;

import javax.swing.table.DefaultTableModel;

import database.MySQL;

public class QueryController extends MySQL{
	public DefaultTableModel denunciantesRead(String where){
		String[] header= {"Nombres", "Apellidos", "C�dula", "Domicilio"};
		String query ="SELECT nombres, apellidos, cedula, domicilio FROM `denunciantes` "+where+" ";
		return read(header, query);
	}
	public DefaultTableModel victimasRead(String where){
		String[] header= {"Nombres", "Apellidos", "C�dula", "Domicilio"};
		String query ="SELECT nombres, apellidos, cedula, domicilio FROM `victimas` "+where+" ";
		return read(header, query);
	}
	public DefaultTableModel denunciasRead(String where){
		String[] header= {"Denunciante", "Victima", "Hechos", "Fecha y hora"};
		String query ="SELECT concat_ws('. ', concat_ws('V-', '', denunciantes.cedula), concat_ws(', ', denunciantes.apellidos, denunciantes.nombres)) AS denunciante, concat_ws('. ', concat_ws('V-', '', victimas.cedula), concat_ws(', ', victimas.apellidos, victimas.nombres)) AS victima, denuncias.hechos, denuncias.fecha FROM `denuncias` INNER JOIN denunciantes ON denunciantes.id = denuncias.denunciante_id INNER JOIN victimas ON victimas.id = denuncias.victima_id WHERE victimas.cedula LIKE '%"+where+"%' ";
		return read(header, query);
	}
	public void denunciaCreate(String denunciante_values, String victima_values, String denuncia_values) throws SQLException{
		String query_denunciante ="INSERT INTO `denunciantes` (`id`, `nombres`, `apellidos`, `cedula`, `domicilio`) VALUES "+denunciante_values+"; ";
		query(query_denunciante);
		String query_victima ="INSERT INTO `victimas` (`id`, `nombres`, `apellidos`, `cedula`, `domicilio`) VALUES "+victima_values+"; ";
		query(query_victima);
		
		String denunciante_id = read(new String[] {"denunciante_id"}, "select id from denunciantes order by id desc limit 1").getValueAt(0,0).toString();
		
		String victima_id = read(new String[] {"denunciante_id"}, "select id from victimas order by id desc limit 1").getValueAt(0,0).toString();
		
		System.out.println("Victima: "+victima_id+", denunciante: "+denunciante_id);
		String query_denuncia ="INSERT INTO `denuncias` (`id`, `denunciante_id`, `victima_id`, `hechos`, `fecha`) VALUES (NULL, "+denunciante_id+", "+victima_id+", "+denuncia_values+"); ";
		query(query_denuncia);
	}
}
